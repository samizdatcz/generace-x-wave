new Tooltip!watchElements!
container = d3.select ig.containers.base
data = d3.tsv.parse ig.data.populace, (row) ->
  for i, d of row
    row[i] = parseInt d, 10
  row

scale = d3.scale.linear!
  ..domain [0 99274]
  ..range [0 50]

f = ig.utils.formatNumber

container.append \div
  ..attr \class \tree
  ..append \div
    ..attr \class "x-line top"
    ..append \span .attr \class "year top" .html "1995"
    ..append \span .attr \class "year bottom" .html "1985"
    ..append \span .attr \class "popisek" .html "Generace Y"
  ..append \div .attr \class \sedmdesatky
    ..append \span .html "Nejsilnější ročník: 1975<br>v České republice 193 224 lidí"
    ..append \div .attr \class \horiz-line
    ..append \div .attr \class \vert-line
  ..selectAll \div.line .data data .enter!append \div
    ..attr \data-tooltip -> "Ročník #{2013 - it.vek}: <b>#{f it.muzi}</b> mužů, <b>#{f it.zeny}</b> žen, dohromady <b>#{f it.muzi + it.zeny}</b> lidí"
    ..attr \class -> "line" + if 18 <= it.vek <= 28 then " x" else " not-x"
    ..append \div
      ..attr \class "muzi bar"
      ..style \width -> "#{scale it.muzi}%"
    ..append \div
      ..attr \class "zeny bar"
      ..style \width -> "#{scale it.zeny}%"
